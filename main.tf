data "docker_registry_image" "Caddy" {
	name = "redserenity/caddy-docker-consul:latest"
}

resource "docker_image" "Caddy" {
	name = data.docker_registry_image.Caddy.name
	pull_triggers = [data.docker_registry_image.Caddy.sha256_digest]
}

module "Caddy" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Caddy.latest

	networks = [{ name: var.docker_network, aliases: [] }]
  ports = [
		{ internal: 80, external: 80, protocol: "tcp" },
		{ internal: 443, external: 443, protocol: "tcp" },
		{ internal: 2019, external: 2019, protocol: "tcp" }
	]
	volumes = [
		{
			host_path = "${var.docker_data}/Caddy/Caddyfile"
			container_path = "/etc/caddy/Caddyfile"
			read_only = false
		},
		{
			host_path = "${var.docker_data}/Caddy/Data"
			container_path = "/data"
			read_only = false
		},
		{
			host_path = "${var.docker_data}/Caddy/Config"
			container_path = "/config"
			read_only = false
		},
		{
			host_path = "${var.docker_data}/Caddy/www"
			container_path = "/www"
			read_only = false
		},
		{
			host_path = "/var/run/docker.sock"
			container_path = "/var/run/docker.sock"
			read_only = true
		},
	]

	environment = {
		"DOCKER_HOST": "unix:///var/run/docker.sock",
		"ACME_CERT_EMAIL": "${var.acme_cert_email}",
		"CONSUL_ADDR": "${var.consul_addr}",
		"ACME_PROVIDER_OPTIONS": "${var.acme_options}"
	}

	stack = var.stack
}