variable "consul_addr" {
	type = string
}

variable "acme_options" {
	type = string
}

variable "acme_cert_email" {
	type = string
}

/* Docker Module Passthrough */
variable "name" {
	type = string
}

variable "docker_network" {
	type = string
}

variable "docker_data" {
	type = string
}

variable "stack" {
	type = string
}